import socket

def start(host, port = 12345):
	# First, create client socket
	s = socket.socket()
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # added allow to reuse when re-run
	s.connect((host, port))

	server_sent = s.recv(1026)
	print(server_sent)

	# Sending data to server
	s.send("Turning right!")

	while True:
		send_msg = input('Type message to server: ')
		s.send(send_msg)
		if send_msg == 'exit':
			break
	s.close()
