import socket
import network

def start(port = 12345):
	host = network.WLAN(network.STA_IF).ifconfig()[0]

	# First, create server socket
	s = socket.socket()
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # added allow to reuse when re-run
	s.bind(('', port))
	s.listen(1)
	print("Waiting for incoming connections on " + host + ":" + str(port))
	c, addr = s.accept()

	print("Client " + addr[0] + " connected through port " + str(addr[1]))
	print("Sending ACK")
	c.send("Message from server to client: connection accepted")

	# Waiting for further data from client
	while True:
		client_sent = c.recv(1026).decode('utf-8')
		if client_sent == 'exit':
			break
		elif client_sent != '':
			print(client_sent)

	print("Closing server socket")
	s.close()
